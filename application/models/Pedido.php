<?php

  class Pedido extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    function insertar($datos){
      return $this->db->insert("pedido",$datos);
    }

    function obtenerTodosListaU(){
      $this->db->select('id_ped, nombre_ped, descripcion_ped,fecha_ped, pais_destino_ped, latitud_destino_ped,longitud_destino_ped,codigo_ped,apellido_cli,nombre_cli, nombre_suc');
      $this->db->from('pedido');
      $this->db->join('cliente', 'cliente.id_cli = pedido.id_fk_cli');
      $this->db->join('sucursal', 'sucursal.id_suc = pedido.id_fk_suc');
      $query = $this->db->get();
      if ($query->num_rows()>0) {
        return  $query->result();
      }else{
        return false;
      }
    }

    function obtenerTodos()
      {
          $listadoPedidos=$this->db->get("pedido");
          if ($listadoPedidos->num_rows()>0) {
            return $listadoPedidos->result();
          }
          return false;
      }
      function buscar($query)
      {
        return $this->db->like('codigo_ped', $query)->get('pedido')->result();
      }
      function borrar($id_ped)
      {
        $this->db->where("id_ped",$id_ped);
        return $this->db->delete("pedido");
      }
  }
?>
