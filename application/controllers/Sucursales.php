<?php

class Sucursales extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        //Cargar modelo
        $this->load->model('Sucursal');
    }

    public function listado()
    {

        //data es un nombre cualquiera
        $data['sucursales'] = $this->Sucursal->obtenerTodos();

        $this->load->view('administradores/header');
        // estamos pasando los datos a la vista 
        $this->load->view('sucursales/listado', $data);
        $this->load->view('administradores/footer');

    }
    public function nuevo()
    {
        $this->load->view('administradores/header');
        $this->load->view('sucursales/nuevo');
        $this->load->view('administradores/footer');
    }

    public function guardar()
    {
        $datosNuevoSucursal = array(
            "nombre_suc" => $this->input->post('nombre_suc'),
            "telefono_suc" => $this->input->post('telefono_suc'),
            "pais_suc" => $this->input->post('pais_suc'),
            "latitud_suc" => $this->input->post('latitud_suc'),
            "longitud_suc" => $this->input->post('longitud_suc'),
            );

        //llamamos a insertar

        if ($this->Sucursal->insertar($datosNuevoSucursal)) {
            redirect('sucursales/listado');
        } else {
            echo "<h1> Error al insertar </h1>";
        }
    }

    public function eliminar($id_suc)
    {
        if ($this->Sucursal->borrar($id_suc)) {
            redirect('sucursales/listado');
            # code...
        } else {
            # code...
            echo "ERROR AL BORRAR :(";
        }

    }

    public function mapa()
    {
        //data es un nombre cualquiera
        $data['sucursales'] = $this->Sucursal->obtenerTodos();

        $this->load->view('administradores/header');
        // estamos pasando los datos a la vista 
        $this->load->view('sucursales/mapa', $data);
        $this->load->view('administradores/footer');

    }

    public function mostrarMapa() {
        $data['paises'] = $this->obtener_paises();
        
        $pais = $this->input->post('pais');
        if (!empty($pais)) {
            $data['sucursales'] = $this->Sucursal->obtener_sucursal_por_pais($pais);
        }

        $this->load->view('administradores/header');
        $this->load->view('sucursales/mostrarMapa', $data);
        $this->load->view('administradores/footer');
        
    }
    
    public function obtener_paises() {
        // Aquí puedes implementar la lógica para obtener la lista de países desde la base de datos o cualquier otra fuente de datos.
        // Por simplicidad, asumiremos que tienes un array fijo de países.
        return array('Ecuador', 'Colombia', 'País 3');
    }


    public function mapaUser()
    {
        //data es un nombre cualquiera
        $data['sucursales'] = $this->Sucursal->obtenerTodos();

        $this->load->view('header');
        // estamos pasando los datos a la vista 
        $this->load->view('sucursales/mapaUser', $data);
        $this->load->view('footer');

    }

    public function mostrarMapaUser() {
        $data['paises'] = $this->obtener_paises();
        
        $pais = $this->input->post('pais');
        if (!empty($pais)) {
            $data['sucursales'] = $this->Sucursal->obtener_sucursal_por_pais($pais);
        }

        $this->load->view('header');
        $this->load->view('sucursales/mostrarMapaUser', $data);
        $this->load->view('footer');
        
    }
} //Cierre d ela clase<

?>