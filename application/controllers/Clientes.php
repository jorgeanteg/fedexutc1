<?php

class Clientes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        //Cargar modelo
        $this->load->model('Cliente');
    }

    public function listado()
    {

        //data es un nombre cualquiera
        $data['clientes'] = $this->Cliente->obtenerTodos();

        $this->load->view('administradores/header');
        // estamos pasando los datos a la vista 
        $this->load->view('clientes/listado', $data);
        $this->load->view('administradores/footer');

    }
    public function nuevo()
    {
        $this->load->view('administradores/header');
        $this->load->view('clientes/nuevo');
        $this->load->view('administradores/footer');
    }

    public function guardar()
    {
        $datosNuevoCliente = array(
            "cedula_cli" => $this->input->post('cedula_cli'),
            "apellido_cli" => $this->input->post('apellido_cli'),
            "nombre_cli" => $this->input->post('nombre_cli'),
            "telefono_cli" => $this->input->post('telefono_cli'),
            "pais_cli" => $this->input->post('pais_cli'),
            "latitud_cli" => $this->input->post('latitud_cli'),
            "longitud_cli" => $this->input->post('longitud_cli'),
        );

        //llamamos a insertar

        if ($this->Cliente->insertar($datosNuevoCliente)) {
            redirect('clientes/listado');
        } else {
            echo "<h1> Error al insertar </h1>";
        }
    }

    public function eliminar($id_cli)
    {
        if ($this->Cliente->borrar($id_cli)) {
            redirect('clientes/listado');
            # code...
        } else {
            # code...
            echo "ERROR AL BORRAR :(";
        }

    }


    public function mapa()
    {

        //data es un nombre cualquiera
        $data['clientes'] = $this->Cliente->obtenerTodos();

        $this->load->view('administradores/header');
        // estamos pasando los datos a la vista 
        $this->load->view('clientes/mapa', $data);
        $this->load->view('administradores/footer');

    }

    public function mostrarMapa() {
        $data['paises'] = $this->obtener_paises();
        
        $pais = $this->input->post('pais');
        if (!empty($pais)) {
            $data['clientes'] = $this->Cliente->obtener_clientes_por_pais($pais);
        }

        $this->load->view('administradores/header');
        $this->load->view('clientes/mostrarMapa', $data);
        $this->load->view('administradores/footer');
        
    }
    
    public function obtener_paises() {
        // Aquí puedes implementar la lógica para obtener la lista de países desde la base de datos o cualquier otra fuente de datos.
        // Por simplicidad, asumiremos que tienes un array fijo de países.
        return array('Ecuador', 'Colombia', 'País 3');
    }

} //Cierre d ela clase<

?>