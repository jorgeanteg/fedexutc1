<?php
class Logins extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Login');
        
    }


    public function login(){
        $this->load->view('header');
        $this->load->view('logins/login');
        $this->load->view('footer');
    }
    
    public function process_login() {
        $this->load->view('header');
        $username = $this->input->post('email_user');
        $password = $this->input->post('password_user');
        
        $user = $this->Login->login($username, $password);

        if ($user) {
            // Inicio de sesión exitoso, redirigir al usuario a la página de inicio
            redirect('administradores/administradores');
        } else {
            // Credenciales inválidas, mostrar mensaje de error
            $data['error'] = 'Credenciales inválidas. Por favor, intenta nuevamente.';
            
            $this->load->view('logins/login', $data);
        }
        $this->load->view('footer');
    }
}
?>
