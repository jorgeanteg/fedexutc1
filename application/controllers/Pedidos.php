<?php

  class Pedidos extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model('Pedido');
    }

    public function nuevopedido(){
      $query=$this->db->get('sucursal');
      $querycli=$this->db->get('cliente');
      $sucursal = $query->result_array();
      $cliente = $querycli->result_array();
      $this->load->view('administradores/header');
      $this->load->view('pedidos/nuevopedido',array('sucursal'=> $sucursal,'cliente'=> $cliente));
      $this->load->view('administradores/footer');
    }

    public function listadopedido(){
      $data['pedidos']=$this->Pedido->obtenerTodosListaU();
      $this->load->view('administradores/header');
      $this->load->view('pedidos/listadopedido',$data);
      $this->load->view('administradores/footer');
    }
    public function destinopedidos(){
      $data['pedidos']=$this->Pedido->obtenerTodos();
      $this->load->view('administradores/header');
      $this->load->view('pedidos/destinopedidos',$data);
      $this->load->view('administradores/footer');
    }

    public function guardarpedido(){
    //codigo neto
    $datosNuevoPedido=array(
      "nombre_ped"=>$this->input->post('nombre_ped'),
      "descripcion_ped"=>$this->input->post('descripcion_ped'),
      "fecha_ped"=>$this->input->post('fecha_ped'),
      "pais_destino_ped"=>$this->input->post('pais_destino_ped'),
      "latitud_destino_ped"=>$this->input->post('latitud_destino_ped'),
      "longitud_destino_ped"=>$this->input->post('longitud_destino_ped'),
      "codigo_ped"=>$this->input->post('codigo_ped'),
      "id_fk_cli"=>$this->input->post('id_fk_cli'),
      "id_fk_suc"=>$this->input->post('id_fk_suc'),
    );
    if ($this->Pedido->insertar($datosNuevoPedido)) {
      redirect('pedidos/listadopedido');
    }else {
        echo "<h1>ERROR AL INSERTAR</h1>";
      }
    }
    public function buscarpedido()
    {
      $query = $this->input->post('query');
      $data['results']=array();
      if ($query) {
        $data['results'] = $this->Pedido->buscar($query);
      }
      $this->load->view('administradores/header');
      $this->load->view('pedidos/buscarpedido',$data);
      $this->load->view('administradores/footer');
    }

    public function buscarpedidocli()
    {
      $query = $this->input->post('query');
      $data['results']=array();
      if ($query) {
        $data['results'] = $this->Pedido->buscar($query);
      }
      $this->load->view('header');
      $this->load->view('pedidos/buscarpedidocli',$data);
      $this->load->view('footer');
    }
    public function eliminar($id_ped){
      if ($this->Pedido->borrar($id_ped))
      {//invocando el modelo
        redirect('pedidos/listadopedido');
      }else{
        echo "ERROR AL BORRAR :()";
      }
    }

  }
?>
