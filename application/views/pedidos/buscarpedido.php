<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <h1>BUSQUEDA DE PEDIDOS</h1>
            <div class="page-body">
                <div class="card">
                    <div class="card-header">
                        <h5>Busqueda de los Pedidos</h5>
                        <br>
                        <span>
                            <div class="col-md-12">
                              <br>
                              <form id="buscar" method="post" action="<?php echo site_url(); ?>/pedidos/buscarpedido">
                                <input type="text" class="form-control" placeholder="Ingrese el codigo del pedido que desea buscar" id="query" name="query">
                                <br>
                                <button type="submit" name="button" class="btn btn-primary" id="buscar" value="BUSCAR">BUSCAR</button>
                              </form>
                            </div>
                        </span>
                          <div class="card-block table-border-style">
                              <div class="table-responsive">
                                  <?php if ($results): ?>
                                    <h2>Resultado de la busqueda</h2>
                                      <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                          <tr>
                                            <th>ID</th>
                                            <th>NOMBRE DEL PEDIDO</th>
                                            <th>DESCRIPCIÓN DEL PEDIDO</th>
                                            <th>FECHA DEL PEDIDO</th>
                                            <th>PAIS DE DESTINO DEL PEDIDO</th>
                                            <th>LATITUD DE DESTINO DEL PEDIDO</th>
                                            <th>LONGITUD DE DESTINO DEL PEDIDO</th>
                                            <th>CÓDIGO DEL PEDIDO</th>
                                            <th>ACCIONES</th>
                                          </tr>
                                        </thead>
                                        <tbody>

                                            <?php foreach ($results as $result): ?>
                                              <tr>
                                                <td>
                                                  <?php echo $result->id_ped ?>
                                                </td>
                                                <td>
                                                  <?php echo $result->nombre_ped ?>
                                                </td>
                                                <td>
                                                  <?php echo $result->descripcion_ped ?>
                                                </td>
                                                <td>
                                                  <?php echo $result->fecha_ped ?>
                                                </td>
                                                <td>
                                                  <?php echo $result->pais_destino_ped ?>
                                                </td>
                                                <td>
                                                  <?php echo $result->latitud_destino_ped ?>
                                                </td>
                                                <td>
                                                  <?php echo $result->longitud_destino_ped ?>
                                                </td>
                                                <td>
                                                  <?php echo $result->codigo_ped ?>
                                                </td>
                                                <td class="text-center">
                                                  <a href= "<?php echo site_url(); ?>/pedidos/eliminar/<?php echo $result->id_ped;?>"
                                                      title="Eliminar Pedido"
                                                      onclick="return confirm('¿Está seguro de eliminar este pedido?');"
                                                      style="color:red;">
                                                      <i class="bi bi-trash-fill"></i>
                                                  </a>

                                                </td>
                                              </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                      </table>
                                      <br>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
            </div>
      </div>
</div>
