<style>
    select option {
        color: #495057;
    }
</style>
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <h1>NUEVO PEDIDO</h1>
            <div class="page-body">
                <div class="card">
                    <div class="card-header">
                        <h5>Datos de los pedidos</h5>
                        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                    </div>
                    <div class="card-block">
                      <form class="" id="frm_nuevo_pedido" action="<?php echo site_url(); ?>/pedidos/guardarpedido" method="post">
                        <div class="container">
                          <div class="row">
                              <div class="col-md-6">
                                <label for="">Nombre del Pedido</label>
                                <br>
                                <input type="text" placeholder="Ingrese el nombre del pedido" class="form-control" name="nombre_ped" value="" id="nombre_ped">
                              </div>
                              <div class="col-md-6">
                                <label for="">Descripción del Pedido</label>
                                <br>
                                <input type="text" placeholder="Ingrese la descripción del pedido" class="form-control" name="descripcion_ped" value="" id="descripcion_ped">
                              </div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-6">
                                <label for="">Fecha de Recepción del Pedido</label>
                                <br>
                                <input type="date" placeholder="Ingrese la fecha de envio del pedido" class="form-control" name="fecha_ped" value="" id="fecha_ped">
                              </div>
                              <div class="col-md-6">
                                <label for="">País de destino del Pedido</label>
                                <br>
                                <select name="pais_destino_ped" id="pais_destino_ped" class="form-control">
                                  <option disabled selected hidden>Seleccione el país desde donde va a enviar el pedido</option>
                                  <option>Ecuador</option>
                                </select>
                              </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="text-center">
                              <label for="">Destino del Pedido</label>
                            </div>
                            <div class="col-md-6">
                              <label for="">Latitud de destino del Pedido</label>
                              <input type="text" placeholder="Ingrese la latitud de destino del pedido" class="form-control" readonly name="latitud_destino_ped" value="" id="latitud_destino_ped">
                            </div>
                            <div class="col-md-6">
                              <label for="">Longitud de destino del Pedido</label>
                              <input type="text" placeholder="Ingrese la longitud de destino del pedido" class="form-control" readonly name="longitud_destino_ped" value="" id="longitud_destino_ped">
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-12">
                              <div id="mapaUbicacion" style="height:300px; width:100%; border:2px solid black;"></div>
                            </div>
                          </div>
                          <script type="text/javascript">
                            function initMap(){
                              var centro=new google.maps.LatLng(-1.2595931997473242, -78.54164276317397);
                              var mapa1=new google.maps.Map(
                                document.getElementById('mapaUbicacion'),
                                {
                                  center:centro,
                                  zoom:7,
                                  mapTypeId:google.maps.MapTypeId.ROADMAP
                                }
                              );

                              var marcador=new google.maps.Marker(
                                {
                                  position:centro,
                                  map:mapa1,
                                  title:"Seleccione la dirección",
                                  icon:"<?php echo base_url() ?>/assets/img/icon2.png",
                                  draggable:true
                                }
                              );
                              google.maps.event.addListener(marcador,'dragend',function(){
                                // alert("Se termino el drag");
                                document.getElementById('latitud_destino_ped').value=this.getPosition().lat();
                                document.getElementById('longitud_destino_ped').value=this.getPosition().lng();
                              });
                            }//cierre de la funcion initMap
                          </script>
                          <br>
                          <div class="row">
                            <div class="col-md-6">
                              <label for="">Codigo del Pedido</label>
                              <input type="text" placeholder="Designe un código para el pedido" class="form-control"   name="codigo_ped" value="" id="codigo_ped">
                            </div>
                            <div class="col-md-6">
                              <label for="">Cliente</label>
                              <br>
                              <select name="id_fk_cli" id="id_fk_cli" class="form-control">
                                <?php foreach ($cliente as $cliente) {?>
                                  <option disabled selected hidden>Seleccione la sucursal desde donde se va a enviar el producto</option>
                                  <option value="<?php echo $cliente['id_cli']; ?>"><?php echo $cliente['apellido_cli'] ?> <?php echo $cliente['nombre_cli'] ?></option>
                                <?php }?>
                              </select>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-12">
                              <label for="">Sucursal</label>
                              <br>
                                <select name="id_fk_suc" id="id_fk_suc" class="form-control">
                                 <?php foreach ($sucursal as $sucursal) {?>
                                  <option disabled selected hidden>Seleccione la sucursal desde donde se va a enviar el producto</option>
                                  <option value="<?php echo $sucursal['id_suc']; ?>"><?php echo $sucursal['nombre_suc']; ?></option>
                                 <?php }?>
                                </select>
                              <br>
                            </div>
                          <br><br>
                          <div class="col-md-12 text-center">
                            <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                            &nbsp
                            <a href="<?php echo site_url(); ?>/pedidos/listadopedido" class="btn btn-danger">Cancelar</a>
                          </div>
                      </div>
                  </form>
                  <script type="text/javascript">
                    $("#frm_nuevo_pedido").validate({
                      rules:{
                        nombre_ped:{
                          required:true,
                          minlength:2,
                          maxlength:100,
                          letras:true
                        },
                        descripcion_ped:{
                          required:true,
                          minlength:3,
                          maxlength:500,
                          letras:true
                        },
                        fecha_ped:{
                          required:true
                        },
                        pais_destino_ped:{
                          required:true
                        },
                        latitud_destino_ped:{
                          required:true
                        },
                        longitud_destino_ped:{
                          required:true
                        },
                        codigo_ped:{
                          required:true,
                          minlength:2,
                          maxlength:20,
                        },
                        id_fk_cli:{
                          required:true,
                        },
                        id_fk_suc:{
                          required:true,
                        },
                      },
                      messages:{
                        nombre_ped:{
                          required:"Por favor ingrese el nombre del pedido",
                          minlength:"El nombre del pedido debe tener al menos 2 caracteres",
                          maxlength:"El nombre del pedido excede el número de caracteres permitidos",
                        },
                        descripcion_ped:{
                          required:"Por favor ingrese la descripción del pedido",
                          minlength:"La descripción del pedido debe tener al menos 3 caracteres",
                          maxlength:"La descripción del pedido excede el número de caracteres permitidos",
                        },
                        fecha_ped:{
                          required:"Por favor seleccione obligatoriamente la fecha de recepción del pedido"
                        },
                        pais_destino_ped:{
                          required:"Por favor debe seleccionar obligatoriamente el país de destino del pedido"
                        },
                        latitud_destino_ped:{
                          required:"Por favor despliegue el marcador en el mapa sobre la ubicación que necesita"
                        },
                        longitud_destino_ped:{
                          required:"Por favor despliegue el marcador en el mapa sobre la ubicación que necesita"
                        },
                        codigo_ped:{
                          required:"Por favor debe asignar obligatoriamente un código al pedido",
                          minlength:"El código del pedido debe tener al menos 2 caracteres",
                          maxlength:"El código del pedido excede el número de caracteres permitidos",
                        },
                        id_fk_cli:{
                          required:"Por favor seleccione el cliente que va a enviar el pedido"
                        },
                        id_fk_suc:{
                          required:"Por favor seleccione la sucursal desde donde se va a enviar el pedido"
                        },
                      }
                    });
                  </script>

              </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>
