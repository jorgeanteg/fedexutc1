<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <h1>LISTADO DE PEDIDOS</h1>
            <div class="page-body">
                <div class="card">
                    <div class="card-header">
                        <h5>Listado de los Pedidos</h5>
                        <span>
                            <div class="col-md-4 ">
                                <a href="<?php echo site_url(); ?>/pedidos/nuevopedido" class="btn btn-primary">
                                    <i class="ti-plus"></i>
                                    Nuevo Pedido
                                </a>
                            </div>
                        </span>
                        <div class="card-header-right">
                            <ul class="list-unstyled card-option">
                                <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                <li><i class="fa fa-minus minimize-card"></i></li>
                                <li><i class="fa fa-refresh reload-card"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-block table-border-style">
                        <div class="table-responsive">
                          <?php if ($pedidos): ?>
                              <table class="table table-striped table-bordered table-hover">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>NOMBRE DEL PEDIDO</th>
                                    <th>DESCRIPCIÓN DEL PEDIDO</th>
                                    <th>FECHA DEL PEDIDO</th>
                                    <th>PAIS DE DESTINO DEL PEDIDO</th>
                                    <th>LATITUD DE DESTINO DEL PEDIDO</th>
                                    <th>LONGITUD DE DESTINO DEL PEDIDO</th>
                                    <th>CÓDIGO DEL PEDIDO</th>
                                    <th>NOMBRES DE CLIENTE QUE ENVIA EL PEDIDO</th>
                                    <th>NOMBRE DE LA SUCURSAL QUE ENVIA EL PEDIDO</th>
                                    <th>ACCIONES</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($pedidos as $filaTemporal): ?>
                                    <tr>
                                      <td>
                                        <?php echo $filaTemporal->id_ped ?>
                                      </td>
                                      <td>
                                        <?php echo $filaTemporal->nombre_ped ?>
                                      </td>
                                      <td>
                                        <?php echo $filaTemporal->descripcion_ped ?>
                                      </td>
                                      <td>
                                        <?php echo $filaTemporal->fecha_ped ?>
                                      </td>
                                      <td>
                                        <?php echo $filaTemporal->pais_destino_ped ?>
                                      </td>
                                      <td>
                                        <?php echo $filaTemporal->latitud_destino_ped ?>
                                      </td>
                                      <td>
                                        <?php echo $filaTemporal->longitud_destino_ped ?>
                                      </td>
                                      <td>
                                        <?php echo $filaTemporal->codigo_ped ?>
                                      </td>
                                      <td>
                                        <?php echo $filaTemporal->apellido_cli ?>
                                        <?php echo $filaTemporal->nombre_cli ?>
                                      </td>
                                      <td>
                                        <?php echo $filaTemporal->nombre_suc ?>
                                      </td>
                                      <td class="text-center">
                                        <a href= "<?php echo site_url(); ?>/pedidos/eliminar/<?php echo $filaTemporal->id_ped;?>"
                                            title="Eliminar Pedido"
                                            onclick="return confirm('¿Está seguro de eliminar este pedido?');"
                                            style="color:red;">
                                            <i class="bi bi-trash-fill"></i>
                                        </a>

                                      </td>
                                    </tr>
                                  <?php endforeach; ?>
                                </tbody>
                              </table>
                              <br>
                            <?php else: ?>
                              <h1 class="text-center">No existen pedidos registrados</h1>
                            <?php endif; ?>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
