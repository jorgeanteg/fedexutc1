<!-- Page Header Start -->
<div class="page-header">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2>Nuestras Sucursales</h2>
			</div>
			<div class="col-12">
				<a href="<?php echo site_url(); ?>/sucursales/mapaUser">General</a>
				<a href="<?php echo site_url(); ?>/sucursales/mostrarMapaUser">Por País</a>
			</div>
		</div>
	</div>
</div>
<!-- Page Header End -->


<!-- Service Start -->
<div class="service">
	<div class="container">
		<div class="section-header text-center">
			<p>Nuestras Sucursales</p>
			<h2>Geolocalización General de las Sucursales</h2>
		</div>
		<div class="row align-items-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header wow fadeInUp" data-wow-delay="0.1s">
						<h5>Sucursales</h5>
						<span>Se presenta las ubicaciones de los Sucursales de todo el mundo</span>
					</div>
					<div class="card-block wow fadeInUp" data-wow-delay="0.2s">
						<div id="mapaSucursal" class="set-map" style="height:500px; width:100%;">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<br>
<!-- Service End -->


<script type="text/javascript">
	function initMap() {
		//Crear el punto central del mapa
		var centro = new google.maps.LatLng(-0.9177264322536244, -78.63301799183898);

		//Creando mapa
		//Tipos de mapa
		//SATELLITE
		//TERRAIN
		var mapaCliente = new google.maps.Map(document.getElementById('mapaSucursal'),
			{ center: centro, zoom: 3, mapTypeId: google.maps.MapTypeId.HYBRID });

		//Marcador 


		//$lugares viene desde el controlador
		<?php if ($sucursales): ?>
			<?php foreach ($sucursales as $lugarTemporal): ?>

				var coordenadaTemporal = new google.maps.LatLng(<?php echo $lugarTemporal->latitud_suc; ?>, <?php echo $lugarTemporal->longitud_suc; ?>);

				var marcadorTemporal = "<?php echo $lugarTemporal->nombre_suc; ?> / <?php echo $lugarTemporal->telefono_suc; ?>";

				var marcador = new google.maps.Marker(
					{
						position: coordenadaTemporal,
						title: marcadorTemporal,
						map: mapaCliente,
						icon: "<?php echo base_url(); ?>/assets/img/sucursal.png"

					});
			<?php endforeach; ?>

		<?php endif; ?>


	}

</script>