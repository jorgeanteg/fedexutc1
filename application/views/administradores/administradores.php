<style>
    .carousel-item {
        height: 90vh;
    }
    .carousel-inner h1 {
        font-size: 70px;
    }
    .carousel-inner p {
        font-size: 40px;
        
    }

    
</style>

<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"
            aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
            aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
            aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="<?php echo base_url(); ?>/assets/img/slider0.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <h1 class="animated fadeInLeft">Conectados con el Mañana</h1>
                <p class="animated fadeInRight">Somos Professionales</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="<?php echo base_url(); ?>/assets/img/slider1.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <h1 class="animated fadeInLeft">Envío seguro de tus Paquetes</h1>
                <p class="animated fadeInRight">Repartidores Profesionales</p>

            </div>
        </div>
        <div class="carousel-item">
            <img src="<?php echo base_url(); ?>/assets/img/slider2.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <h1 class="animated fadeInLeft">Cobertura Internacional</h1>
                <p class="animated fadeInRight">Somos de confianza</p>
            </div>
        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>