<!-- Carousel Start -->
<div id="carousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carousel" data-slide-to="0" class="active"></li>
        <li data-target="#carousel" data-slide-to="1"></li>
        <li data-target="#carousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="<?php echo base_url(); ?>/assets/img/slider0.jpg" alt="Carousel Image">
            <div class="carousel-caption">
                <p class="animated fadeInRight">Somos Professionales</p>
                <h1 class="animated fadeInLeft">Conectados con el Mañana</h1>
                <a class="btn animated fadeInUp" href="https://htmlcodex.com/construction-company-website-template">Rastrea tu Pedido</a>
            </div>
        </div>

        <div class="carousel-item">
            <img src="<?php echo base_url(); ?>/assets/img/slider1.jpg" alt="Carousel Image">
            <div class="carousel-caption">
                <p class="animated fadeInRight">Repartidores Profesionales</p>
                <h1 class="animated fadeInLeft">Envío seguro de tus Paquetes</h1>
                <a class="btn animated fadeInUp" href="https://htmlcodex.com/construction-company-website-template">Rastrea tu Pedido</a>
            </div>
        </div>

        <div class="carousel-item">
            <img src="<?php echo base_url(); ?>/assets/img/slider2.jpg" alt="Carousel Image">
            <div class="carousel-caption">
                <p class="animated fadeInRight">Somos de confianza</p>
                <h1 class="animated fadeInLeft">Cobertura Internacional</h1>
                <a class="btn animated fadeInUp" href="https://htmlcodex.com/construction-company-website-template">Rastrea tu Pedido</a>
            </div>
        </div>
    </div>

    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- Carousel End -->
<!-- Feature Start-->
<div class="feature wow fadeInUp" data-wow-delay="0.1s">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-4 col-md-12">
                <div class="feature-item">
                    <div class="feature-icon">
                        <i class="flaticon-worker"></i>
                    </div>
                    <div class="feature-text">
                        <h3>Trabajadores Expertos</h3>
                        <p>Lorem ipsum dolor sit amet elit. Phasus nec pretim ornare velit non</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="feature-item">
                    <div class="feature-icon">
                        <i class="flaticon-building"></i>
                    </div>
                    <div class="feature-text">
                        <h3>Envíos de Calidad</h3>
                        <p>Lorem ipsum dolor sit amet elit. Phasus nec pretim ornare velit non</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="feature-item">
                    <div class="feature-icon">
                        <i class="flaticon-call"></i>
                    </div>
                    <div class="feature-text">
                        <h3>Soporte 24/7</h3>
                        <p>Lorem ipsum dolor sit amet elit. Phasus nec pretim ornare velit non</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Feature End-->