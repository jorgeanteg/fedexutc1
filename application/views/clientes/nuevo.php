<style>
    select option {
        color: #495057;
    }
</style>



<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <h1>NUEVO CLIENTE</h1>
            <div class="page-body">
                <div class="card">
                    <div class="card-header">
                        <h5>Datos de los clientes</h5>
                        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                    </div>
                    <div class="card-block">

                        <form class="" action="<?php echo site_url(); ?>/clientes/guardar" method="post">
                            <div class="row">

                                <div class="col-md-6">
                                    <label for="">Cédula:</label>
                                    <br>
                                    <input type="number" placeholder="Ingrese la cédula" class="form-control"
                                        name="cedula_cli" value="" id="cedula_cli">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Apellidos:</label>
                                    <br>
                                    <input type="text" placeholder="Ingrese sus apellidos" class="form-control"
                                        name="apellido_cli" value="" id="apellido_cli">
                                </div>
                            </div>
                            <br>
                            <div class="row">

                                <div class="col-md-6">
                                    <label for="">Nombres:</label>
                                    <br>
                                    <input type="text" placeholder="Ingrese los nombres" class="form-control"
                                        name="nombre_cli" value="" id="nombre_cli">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Teléfono:</label>
                                    <br>
                                    <input type="text" placeholder="Ingrese el titulo" class="form-control"
                                        name="telefono_cli" value="" id="telefono_cli">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">País:</label>
                                    <br>
                                    <select class="form-select" type="text" name="pais_cli" id="pais_cli">
                                    <option selected disabled>Selecione su País</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Perú">Perú</option>
                                </select>
                                </div>

                                

                            </div>

                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for=""><strong>Dirección</strong></label>
                                </div>

                                <br>
                                <div class="col-md-6">
                                    <label for="">Latitud:</label>
                                    <br>
                                    <input type="number" placeholder="Ingrese la direccion" class="form-control"
                                        readonly name="latitud_cli" value="" id="latitud_cli">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Longitud:</label>
                                    <br>
                                    <input type="number" placeholder="Ingrese la direccion" class="form-control"
                                        readonly name="longitud_cli" value="" id="longitud_cli">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <br>
                                    <div id="mapaUbicacion" style="height:350px; width:100%; border:2px solid black;">

                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" name="button" class="btn btn-primary">
                                        Guardar
                                    </button>
                                    &nbsp;
                                    <a href="<?php echo site_url(); ?>/clientes/listado" class="btn btn-danger">
                                        Cancelar
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<script type="text/javascript">
    function initMap() {
        //Crear el punto central del mapa
        var centro = new google.maps.LatLng(-1.6654563910937459, -78.66144701362455);

        //Creando mapa
        //Tipos de mapa
        //SATELLITE
        //TERRAIN
        var mapa1 = new google.maps.Map(document.getElementById('mapaUbicacion'),
            { center: centro, zoom: 6, mapTypeId: google.maps.MapTypeId.HYBRID });


        var marcador = new google.maps.Marker(
            {
                position: centro,
                map: mapa1,
                title: "Seleccione la dirección",
                icon: "<?php echo base_url(); ?>/assets/img/icon2.png",
                draggable: true

            });

        google.maps.event.addListener(marcador, 'dragend', function () {
            // alert("Se termino el drag");
            document.getElementById('latitud_cli').value = this.getPosition().lat();
            document.getElementById('longitud_cli').value = this.getPosition().lng();
        });
    }
</script>
</div>