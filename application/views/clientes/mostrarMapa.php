<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <h1>Geolocalización de los Clientes por País</h1>
            <!-- Page body start -->
            <div class="page-body">
                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            <form action="<?= site_url('clientes/mostrarMapa') ?>" method="post">
                                
                                <select class="form-select" id="pais" name="pais" onchange="this.form.submit()">
                                    <option value="">Seleccione un país</option>
                                    <?php foreach ($paises as $pais): ?>
                                        <option value="<?= $pais ?>" <?= ($pais == $this->input->post('pais')) ? 'selected' : '' ?>><?= $pais ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!-- Basic map start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Clientes</h5>
                                <span>Se presenta las ubicaciones de los clientes segun el país seleccionado</span>
                            </div>
                            <div class="card-block">
                                <div id="mapa" class="set-map" style="height:500px; width:100%;">
                                </div>
                            </div>
                        </div>
                        <!-- Basic map end -->
                    </div>

                </div>
            </div>
            <!-- Page body end -->
        </div>
    </div>
</div>

<script>
    var map;
    var markers = [];

    function agregarMarcador(nombre_cli, apellido_cli, latitud_cli, longitud_cli) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(latitud_cli, longitud_cli),
            map: map,
            title: nombre_cli + ' ' + apellido_cli,
            icon: "<?php echo base_url(); ?>/assets/img/cliente1.png"
        });

        markers.push(marker);
    }

    function limpiarMarcadores() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    }

    function centrarMapa() {
        if (markers.length > 0) {
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < markers.length; i++) {
                bounds.extend(markers[i].getPosition());
            }
            map.fitBounds(bounds);
        }
    }

    function initMap() {
        map = new google.maps.Map(document.getElementById('mapa'), {
            center: { lat: -0.9177264322536244, lng: -78.63301799183898 },
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.HYBRID
        });

        <?php if (!empty($clientes)): ?>
            <?php foreach ($clientes as $cliente): ?>
                agregarMarcador('<?= $cliente->nombre_cli ?>', '<?= $cliente->apellido_cli ?>', <?= $cliente->latitud_cli ?>, <?= $cliente->longitud_cli ?>);
            <?php endforeach; ?>
            centrarMapa();
        <?php endif; ?>
    }

    document.addEventListener("DOMContentLoaded", function () {
        initMap();
    });

</script>


</body>

</html>