<style>

    .contact-item img {
        width: 100%;
        height: 400px;
    }
    .imagen_login {
        width: 200px;
    }
</style>
<!-- Contact Start -->
<div class="contact wow fadeInUp">
    <div class="container">
        <div class="section-header text-center">
            <h2>Iniciar Sesión</h2>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="contact-info">
                    <div class="contact-item">
                        <img src="<?php echo base_url(); ?>/assets/img/slider0.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact-form">
                    <div id="success"></div>
                    <?php if (isset($error)) { ?>
                        <p>
                            <?php echo $error; ?>
                        </p>
                    <?php } ?>
                    <form action="<?php echo site_url(); ?>/logins/process_login" method="post" id="contactForm"
                        novalidate="novalidate">
                        <div class="control-group text-center">
                            <img class="imagen_login" src="<?php echo base_url(); ?>/assets/img/login1.png"  alt="">
                        </div>
                        <div class="control-group">
                            <label for="username">Usuario:</label>
                            <input class="form-control" type="email" name="email_user" id="email_user" required />
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="control-group">
                            <label for="password">Contraseña:</label>
                            <input class="form-control" type="password" name="password_user" id="password_user"
                                required />
                            <p class="help-block text-danger"></p>
                        </div>

                        <div class="text-center" >
                            <button class="btn" type="submit" value="Iniciar sesión">Ingresar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact End -->